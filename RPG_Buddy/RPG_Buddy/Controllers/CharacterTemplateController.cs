﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RPG_Buddy.Models;

namespace RPG_Buddy
{
    public class CharacterTemplateController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: CharacterTemplate
        public ActionResult Index()
        {
            return View(db.CharacterTemplates.ToList());
        }

        // GET: CharacterTemplate/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CharacterTemplate characterTemplate = db.CharacterTemplates.Find(id);
            if (characterTemplate == null)
            {
                return HttpNotFound();
            }
            return View(characterTemplate);
        }

        // GET: CharacterTemplate/Create
        public ActionResult Create()
        {
            var rulesets = db.Rulesets;
            IList<Ruleset> rList = new List<Ruleset>();
            foreach (Ruleset r in rulesets) {
                rList.Add(r);
            }


            ViewBag.rList = rList;
            return View();
        }

        // POST: CharacterTemplate/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Schema,Name")] CharacterTemplate characterTemplate)
        {
            if (ModelState.IsValid)
            {
                db.CharacterTemplates.Add(characterTemplate);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(characterTemplate);
        }

        // GET: CharacterTemplate/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CharacterTemplate characterTemplate = db.CharacterTemplates.Find(id);
            if (characterTemplate == null)
            {
                return HttpNotFound();
            }
            return View(characterTemplate);
        }

        // POST: CharacterTemplate/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Schema,Name")] CharacterTemplate characterTemplate)
        {
            if (ModelState.IsValid)
            {
                db.Entry(characterTemplate).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(characterTemplate);
        }

        // GET: CharacterTemplate/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CharacterTemplate characterTemplate = db.CharacterTemplates.Find(id);
            if (characterTemplate == null)
            {
                return HttpNotFound();
            }
            return View(characterTemplate);
        }

        // POST: CharacterTemplate/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CharacterTemplate characterTemplate = db.CharacterTemplates.Find(id);
            db.CharacterTemplates.Remove(characterTemplate);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
