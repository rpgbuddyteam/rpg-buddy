﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RPG_Buddy.Models;
using System.Diagnostics;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace RPG_Buddy.Controllers
{
    public class ItemCardController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ItemCard
        public ActionResult Index()
        {
            return View(db.ItemCards.ToList());
        }

        // GET: ItemCard/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItemCard ItemCard = db.ItemCards.Find(id);
            if (ItemCard == null)
            {
                return HttpNotFound();
            }
            return View(ItemCard);
        }

        // GET: ItemCard/Create
        public ActionResult Create()
        {
            var model = new ItemCardViewModel();
            model.ItemTemplateNames = db.ItemTemplates.Select(x => x.Name).ToList();
            return View(model);
        }

        // POST: ItemCard/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Create(object obj)
        {
            var keys = Request.Form.AllKeys;

            var ItemCard = new ItemCard();
            ItemCard.Name = Request.Form[keys[0]];

            var formValues = new List<ItemCardFormValue>();

            for (int i = 1; i < keys.Length; i++)
            {
                formValues.Add(new ItemCardFormValue
                {
                    Name = keys[i],
                    Value = Request.Form[keys[i]]
                });
            }
            //walidacja
            for (int i = 1; i < (formValues.Count); i++)
            {

                int value;
                if (int.TryParse(formValues[i].Value, out value))
                {
                    //OK
                }
                else
                    formValues[i].Value = "0";

            }
            ItemCard.ItemCardFormValues = formValues;
            db.ItemCards.Add(ItemCard);
            db.SaveChanges();

            return RedirectToAction("Index");
        }
        [HttpPost]
        public JsonResult GetTemplate(string name)
        {
            var result = db.ItemTemplates.FirstOrDefault(x => x.Name == name);
            return Json(result);
        }

        // GET: ItemCard/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItemCard ItemCard = db.ItemCards.Find(id);
            ItemCardViewModel vm = new ItemCardViewModel();
            if (ItemCard == null)
            {
                return HttpNotFound();
            }
            return View(ItemCard);
        }

        // POST: ItemCard/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.

        //public ActionResult Edit([Bind(Include = "Id,ImieNazwisko,Narodowosc,Rasa,Profesja,Archetyp,Krzepa,Zrecznosc,Przenikliwosc,Opanowanie,Charyzma,Umiejetnosci")] ItemCard ItemCard)

       /* public ActionResult Edit(ItemCard ItemCard)
        {
            var r = Request;
            var form = r.Form;
            var keys = Request.Form.AllKeys;
            var data = new List<string>();

            for (var i = 1; i < keys.Count(); i++) {
                data.Add(r[keys[i]]);
            }

            if (ModelState.IsValid)
            {
                db.Entry(ItemCard).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(ItemCard);
        }*/

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(string Name, int Id, string[] formItem)
        {

            var r = Request;

            ItemCard itemCard = db.ItemCards.Find(Id);
            if (ModelState.IsValid)
            {
                itemCard.Name = Name;
                var fItem = Request["formItem.Value"];
                var items = fItem.Split(',');
                var values = items.Select(i => i).ToList();

                //db.Entry(characterCard).State = EntityState.Deleted; 
                //db.Entry(characterCard).State = EntityState.Modified;

                //characterCard.CharacterCardFormValues.Clear();

                //db.Entry(characterCard.CharacterCardFormValues).

                //db.SaveChanges();
                //characterCard.CharacterCardFormValues = values;
                //db.Entry(characterCard).State = EntityState.Modified;
                //TryUpdateModel(characterCard);
                //db.Entry(characterCard).State = EntityState.Modified;

                for (var i = 0; i < itemCard.ItemCardFormValues.Count; i++)
                {

                    itemCard.ItemCardFormValues[i].Value = values[i];

                }

                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(itemCard);
        }


        // GET: ItemCard/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItemCard ItemCard = db.ItemCards.Find(id);
            if (ItemCard == null)
            {
                return HttpNotFound();
            }
            return View(ItemCard);
        }

        // POST: ItemCard/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ItemCard ItemCard = db.ItemCards.Find(id);
            db.ItemCards.Remove(ItemCard);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        // /ItemCard/Vote/?itemId=1&Rating=10
        public ActionResult Vote(int itemId, int rating=1) {

            //ItemCard ic = db.ItemCards.Find(itemId);

            //string userId = User.Identity.GetUserId();

            ApplicationUser user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());
            string userId = user.Id;

            if (userId != null)
            {

                ItemVote iv = new ItemVote(rating, itemId, userId);

                // var votes = from v in db.ItemVotes select  
                var votes = db.ItemVotes.Where(v => v.ItemCardId == itemId && v.UserId == userId);
                if (votes.Count() == 0)
                {
                    db.ItemVotes.Add(iv);
                    ItemCard item = db.ItemCards.Find(itemId);
                    item.Votes.Add(iv);
                    db.Entry(item).State = EntityState.Modified;
                    db.SaveChanges();

                    //TODO: make sure item has not been rated by this user before
                    ViewBag.rating = rating;
                    ViewBag.querystatus = "OK";
                    return View("Vote");
                }
               else {

                    if (votes.Count() == 1)
                    {
                        var oldVote = votes.First();
                        if (oldVote.Value == rating) { return View("VotedAlready"); }
                        oldVote.Value = rating;
                        db.Entry(oldVote).State = EntityState.Modified;
                        db.SaveChanges();
                        ViewBag.rating = rating;
                        ViewBag.querystatus = "OK";
                        return View("Vote");
                    }

                    /*   ViewBag.querystatus = "FAILURE";
                       return View("Vote");*/
                    return View("VotedAlready");
                }

            }
            ViewBag.querystatus = "FAILURE";
            return View("Vote");

        }

        public int getTotalRating(int Id)
        {

            var votes = db.ItemVotes.Where(v => v.ItemCardId == Id).ToList();
            if (votes.Count != 0)
            {
                var sum = votes.Sum(v => v.Value);
                return sum;
            }
            else return 0;
            
        }

        /*public ActionResult Upvote()
        {


            
            int votePower = 1;

            

            List<int> upvotedList;
            int idFromSession = (int)Session["id"];



            if (Session["downvoted"] != null)
            {
                List<int> dList = (List<int>)Session["downvoted"];
                if (dList.Contains(idFromSession))
                {
                    votePower = 2;
                }
            }


            if (Session["upvoted"] != null)
            {
                upvotedList = (List<int>)Session["upvoted"];
                if (upvotedList.Contains(idFromSession))
                {
                    return View("VotedAlready");
                }
                else
                {

                    //ViewBag.id = null;

                    ItemCard card = db.ItemCards
              .Where(c => c.Id == idFromSession).FirstOrDefault();

                    //card.Rating += 1;
                    card.Votes += votePower;

            

                    db.SaveChanges();

                    upvotedList = (List<int>)Session["upvoted"];
                    upvotedList.Add(idFromSession);
                    Session["upvoted"] = upvotedList;

                    //return RedirectToAction("Index", );
                    return View("Upvote");

                }

            }
            else
            {// list is null

                //ViewBag.id = null;
                ItemCard card = db.ItemCards
              .Where(c => c.Id == idFromSession).FirstOrDefault();

                //card.Rating += 1;
                

                card.Votes += votePower;

                db.SaveChanges();

                //upvotedList = (List<int>)Session["upvoted"];
                upvotedList = new List<int>();
                upvotedList.Add(idFromSession);
                Session["upvoted"] = upvotedList;

                //return RedirectToAction("Index", );
                return View("Upvote");
            }
        }



        public ActionResult Downvote()
        {
            int votePower = 1;



            List<int> downvotedList;
            int idFromSession = (int)Session["id"];



            if (Session["upvoted"] != null)
            {
                List<int> dList = (List<int>)Session["upvoted"];
                if (dList.Contains(idFromSession))
                {
                    votePower = 2;
                }
            }


            if (Session["downvoted"] != null)
            {
                downvotedList = (List<int>)Session["downvoted"];
                if (downvotedList.Contains(idFromSession))
                {
                    return View("VotedAlready");
                }
                else
                {

                    //ViewBag.id = null;

                    ItemCard card = db.ItemCards
              .Where(c => c.Id == idFromSession).FirstOrDefault();

                    //card.Rating += 1;
                    card.Votes -= votePower;

                    db.SaveChanges();

                    downvotedList = (List<int>)Session["downvoted"];
                    downvotedList.Add(idFromSession);
                    Session["downvoted"] = downvotedList;

                    //return RedirectToAction("Index", );
                    return View("Upvote");

                }

            }
            else
            {// list is null

                //ViewBag.id = null;
                ItemCard card = db.ItemCards
              .Where(c => c.Id == idFromSession).FirstOrDefault();

                //card.Rating += 1;


                card.Votes += votePower;

                db.SaveChanges();

                //upvotedList = (List<int>)Session["upvoted"];
                downvotedList = new List<int>();
                downvotedList.Add(idFromSession);
                Session["downvoted"] = downvotedList;

                //return RedirectToAction("Index", );
                return View("Downvote");
            }
        }


        /// <summary>
        /// ////////////////////
        /// </summary>
        /// <returns></returns>
/*
      public ActionResult DownvoteOLD()
        {

            int votePower = 2;

            List<int> downvotedList;
            int idFromSession = (int)Session["id"];

            if (Session["downvoted"] != null)
            {
                downvotedList = (List<int>)Session["downvoted"];
                if (downvotedList.Contains(idFromSession))
                {
                    return View("VotedAlready");
                }
                else
                {

                    //ViewBag.id = null;
                    ItemCard card = db.ItemCards
                  .Where(c => c.Id == idFromSession).FirstOrDefault();

                    //card.Rating += 1;

                    if (Session["upvoted"] != null)
                    {
                        List<int> dList = (List<int>)Session["upvoted"];
                        if (dList.Contains(card.Id))
                        {
                            votePower = 2;
                        }
                    }

                    card.Votes -= votePower;

                    db.SaveChanges();

                    downvotedList = (List<int>)Session["downvoted"];
                    downvotedList.Add(idFromSession);
                    Session["downvoted"] = downvotedList;

                    //return RedirectToAction("Index", );
                    return View("Downvote");

                }

            }
            else
            {

                //ViewBag.id = null;
                ItemCard card = db.ItemCards
              .Where(c => c.Id == idFromSession).FirstOrDefault();

                //card.Rating += 1;
                card.Votes -= 1;

                db.SaveChanges();

                downvotedList = (List<int>)Session["upvoted"];
                downvotedList.Add(idFromSession);
                Session["downvoted"] = downvotedList;

                //return RedirectToAction("Index", );
                return View("Downvote");
            }
        }
        */
    }
}