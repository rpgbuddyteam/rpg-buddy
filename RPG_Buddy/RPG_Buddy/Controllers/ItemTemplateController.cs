﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RPG_Buddy.Models;

namespace RPG_Buddy
{
    public class ItemTemplateController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ItemTemplate
        public ActionResult Index()
        {
            return View(db.ItemTemplates.ToList());
        }

        // GET: ItemTemplate/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItemTemplate itemTemplate = db.ItemTemplates.Find(id);
            if (itemTemplate == null)
            {
                return HttpNotFound();
            }
            return View(itemTemplate);
        }

        // GET: ItemTemplate/Create
        public ActionResult Create()
        {
			
			var rulesets = db.Rulesets;
            IList<Ruleset> rList = new List<Ruleset>();
            foreach (Ruleset r in rulesets) {
                rList.Add(r);
            }


            ViewBag.rList = rList;
			
            return View();
        }

        // POST: ItemTemplate/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Schema,Name")] ItemTemplate itemTemplate)
        {
            if (ModelState.IsValid)
            {
                db.ItemTemplates.Add(itemTemplate);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(itemTemplate);
        }

        // GET: ItemTemplate/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItemTemplate itemTemplate = db.ItemTemplates.Find(id);
            if (itemTemplate == null)
            {
                return HttpNotFound();
            }
            return View(itemTemplate);
        }

        // POST: ItemTemplate/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Schema,Name")] ItemTemplate itemTemplate)
        {
            if (ModelState.IsValid)
            {
                db.Entry(itemTemplate).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(itemTemplate);
        }

        // GET: ItemTemplate/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItemTemplate itemTemplate = db.ItemTemplates.Find(id);
            if (itemTemplate == null)
            {
                return HttpNotFound();
            }
            return View(itemTemplate);
        }

        // POST: ItemTemplate/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ItemTemplate itemTemplate = db.ItemTemplates.Find(id);
            db.ItemTemplates.Remove(itemTemplate);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
