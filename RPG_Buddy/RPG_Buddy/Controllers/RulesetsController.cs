﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RPG_Buddy.Models;

namespace RPG_Buddy
{
    public class RulesetsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Rulesets
        public ActionResult Index()
        {
            return View(db.Rulesets.ToList());
        }

        // GET: Rulesets/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ruleset ruleset = db.Rulesets.Find(id);
            if (ruleset == null)
            {
                return HttpNotFound();
            }
            return View(ruleset);
        }

        // GET: Rulesets/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Rulesets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Description")] Ruleset ruleset)
        {
            if (ModelState.IsValid)
            {
                db.Rulesets.Add(ruleset);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(ruleset);
        }

        // GET: Rulesets/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ruleset ruleset = db.Rulesets.Find(id);
            if (ruleset == null)
            {
                return HttpNotFound();
            }
            return View(ruleset);
        }

        // POST: Rulesets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Description")] Ruleset ruleset)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ruleset).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(ruleset);
        }

        // GET: Rulesets/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ruleset ruleset = db.Rulesets.Find(id);
            if (ruleset == null)
            {
                return HttpNotFound();
            }
            return View(ruleset);
        }

        // POST: Rulesets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Ruleset ruleset = db.Rulesets.Find(id);
            db.Rulesets.Remove(ruleset);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
