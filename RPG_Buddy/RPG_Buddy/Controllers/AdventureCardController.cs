﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RPG_Buddy.Models;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity.EntityFramework;

namespace RPG_Buddy.Controllers
{
    public class AdventureCardController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationUserManager _userManager;


        public AdventureCardController()
        {
        }

        public AdventureCardController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: AdventureCard
        public ActionResult Index()
        {
            return View(db.AdventureCards.ToList());
        }

        // GET: AdventureCard/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AdventureCard adventureCard = db.AdventureCards.Find(id);
            if (adventureCard == null)
            {
                return HttpNotFound();
            }

            ViewBag.nr1 = adventureCard.Postać1;
            ViewBag.nr2 = adventureCard.Postać2;
            ViewBag.nr3 = adventureCard.Postać3;
            ViewBag.nr4 = adventureCard.Postać4;
            ViewBag.nr5 = adventureCard.Przedmiot1;
            ViewBag.nr6 = adventureCard.Przedmiot2;
            ViewBag.nr7 = adventureCard.Przedmiot3;
            ViewBag.nr8 = adventureCard.Przedmiot4;
            ViewBag.nr9 = adventureCard.Przedmiot5;
            ViewBag.nr10 = adventureCard.Przedmiot6;
            ViewBag.nr11 = adventureCard.Przedmiot7;
            ViewBag.nr12 = adventureCard.Przedmiot8;
            ViewBag.nr13 = adventureCard.Przedmiot9;
            ViewBag.nr14 = adventureCard.Przedmiot10;

            return View(adventureCard);
        }
        public async Task<ActionResult> Detailsx(string name)
        {
            if (name == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = await UserManager.FindByNameAsync(name);
            return View(user);
        }
        public ActionResult Detailsy(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItemCard ItemCard = db.ItemCards.Find(id);
            ItemCardViewModel vm = new ItemCardViewModel();
            if (ItemCard == null)
            {
                return HttpNotFound();
            }
            return View(ItemCard);
        }


        // GET: AdventureCard/Create
        public ActionResult Create()
        {
            var allitems = db.ItemCards.ToList();
            var allusers = db.Users.ToList();

            var users = allusers.ToList();
            var model = new AdventureCard { Users = allusers , Items = allitems};

            return View(model);
        }

        // POST: AdventureCard/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,OpisPrzygody,Postać1,Postać2,Postać3,Postać4,Przedmiot1,Przedmiot2,Przedmiot3,Przedmiot4,Przedmiot5,Przedmiot6,Przedmiot7,Przedmiot8,Przedmiot9,Przedmiot10,Users,Items")] AdventureCard adventureCard)
        {
            var allusers = db.Users.ToList();
            var allitems = db.ItemCards.ToList();
            adventureCard.Users = allusers;
            adventureCard.Items = allitems;
            if (ModelState.IsValid)
            {
                db.AdventureCards.Add(adventureCard);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(adventureCard);
        }

        // GET: AdventureCard/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AdventureCard adventureCard = db.AdventureCards.Find(id);
            if (adventureCard == null)
            {
                return HttpNotFound();
            }
            return View(adventureCard);
        }

        // POST: AdventureCard/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,OpisPrzygody,Postać1,Postać2,Postać3,Postać4")] AdventureCard adventureCard)
        {
            if (ModelState.IsValid)
            {
                db.Entry(adventureCard).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(adventureCard);
        }

        // GET: AdventureCard/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AdventureCard adventureCard = db.AdventureCards.Find(id);
            if (adventureCard == null)
            {
                return HttpNotFound();
            }
            return View(adventureCard);
        }

        // POST: AdventureCard/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AdventureCard adventureCard = db.AdventureCards.Find(id);
            db.AdventureCards.Remove(adventureCard);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
