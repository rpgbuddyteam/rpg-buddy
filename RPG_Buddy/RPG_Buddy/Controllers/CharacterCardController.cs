﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RPG_Buddy.Models;
using System.Diagnostics;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;

namespace RPG_Buddy
{
    public class CharacterCardController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: CharacterCard
        public ActionResult Index()
        {
            return View(db.CharacterCards.ToList());
        }

        // GET: CharacterCard/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CharacterCard characterCard = db.CharacterCards.Find(id);
            if (characterCard == null)
            {
                return HttpNotFound();
            }
            return View(characterCard);
        }

        // GET: CharacterCard/Create
        public ActionResult Create()
        {
            var model = new CharacterCardViewModel();
            model.CharacterTemplateNames = db.CharacterTemplates.Select(x=> x.Name).ToList();
            return View(model);
        }

        // POST: CharacterCard/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Create(object obj)
        {
            var keys = Request.Form.AllKeys;

            var characterCard = new CharacterCard();
            characterCard.Name = Request.Form[keys[0]];

            var formValues = new List<CharacterCardFormValue>();
           
            for (int i = 1; i < keys.Length; i++)
            {
                formValues.Add(new CharacterCardFormValue
                {
                    Name = keys[i],
                    Value = Request.Form[keys[i]]
                });
            }
            characterCard.CharacterCardFormValues = formValues;
            db.CharacterCards.Add(characterCard);
            db.SaveChanges();

            return RedirectToAction("Index");
        }
        [HttpPost]
        public JsonResult GetTemplate(string name)
        {
            var result = db.CharacterTemplates.FirstOrDefault(x => x.Name == name);
            return Json(result);
        }

        // GET: CharacterCard/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                //id = ViewBag.id;
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
            CharacterCard characterCard = db.CharacterCards.Find(id);
            CharacterCardViewModel vm = new CharacterCardViewModel();
            if (characterCard == null)
            {
                return HttpNotFound();
            }

            Session["id"] = id;

          
            return View(characterCard);
        }


        public ActionResult Vote(int characterCardId, int rating = 1)
        {

            //ItemCard ic = db.ItemCards.Find(itemId);

            //string userId = User.Identity.GetUserId();

            ApplicationUser user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());
            string userId = user.Id;

            if (userId != null)
            {

                CharacterVote characterVote = new CharacterVote(rating, characterCardId, userId);

                // var votes = from v in db.ItemVotes select  
                var votes = db.CharacterVotes.Where(v => v.CharacterVoteId == characterCardId && v.UserId == userId);
                if (votes.Count() == 0)
                {
                    db.CharacterVotes.Add(characterVote);
                    CharacterCard character = db.CharacterCards.Find(characterCardId);
                    character.Votes.Add(characterVote);
                    db.Entry(character).State = EntityState.Modified;
                    db.SaveChanges(); ;

                    //TODO: make sure item has not been rated by this user before
                    ViewBag.rating = rating;
                    ViewBag.querystatus = "OK";
                    return View("Vote");
                }
                else
                {
                    if (votes.Count() == 1) {
                        var oldVote = votes.First();

                        if (oldVote.Value == rating) { return View("VotedAlready"); }

                        oldVote.Value = rating;
                        db.Entry(oldVote).State = EntityState.Modified;
                        db.SaveChanges();
                        ViewBag.rating = rating;
                        ViewBag.querystatus = "OK";
                        return View("Vote");
                    }

                    /*   ViewBag.querystatus = "FAILURE";
                       return View("Vote");*/
                    return View("VotedAlready");
                }

            }
            ViewBag.querystatus = "FAILURE";
            return View("Vote");

        }

        public int getTotalRating(int Id) {

            var votes = db.CharacterVotes.Where(v => v.CharacterCardId == Id).ToList();
            if (votes.Count != 0)
            {
                var sum = votes.Sum(v => v.Value);
                return sum;
            }
            else return 0;
        }

        /*public ActionResult Upvote()
        {
            int votePower = 1;



            List<int> upvotedList;
            int idFromSession = (int)Session["id"];



            if (Session["downvoted"] != null)
            {
                List<int> dList = (List<int>)Session["downvoted"];
                if (dList.Contains(idFromSession))
                {
                    votePower = 2;
                }
            }


            if (Session["upvoted"] != null)
            {
                upvotedList = (List<int>)Session["upvoted"];
                if (upvotedList.Contains(idFromSession))
                {
                    return View("VotedAlready");
                }
                else
                {

                    //ViewBag.id = null;

                    CharacterCard card = db.CharacterCards
              .Where(c => c.Id == idFromSession).FirstOrDefault();

                    //card.Rating += 1;
                    card.Votes += votePower;



                    db.SaveChanges();

                    upvotedList = (List<int>)Session["upvoted"];
                    upvotedList.Add(idFromSession);
                    Session["upvoted"] = upvotedList;

                    //return RedirectToAction("Index", );
                    return View("Upvote");

                }

            }
            else
            {// list is null

                //ViewBag.id = null;
                CharacterCard card = db.CharacterCards
              .Where(c => c.Id == idFromSession).FirstOrDefault();

                //card.Rating += 1;


                card.Votes += votePower;

                db.SaveChanges();

                //upvotedList = (List<int>)Session["upvoted"];
                upvotedList = new List<int>();
                upvotedList.Add(idFromSession);
                Session["upvoted"] = upvotedList;

                //return RedirectToAction("Index", );
                return View("Upvote");
            }
        }



        public ActionResult Downvote()
        {
            int votePower = 1;



            List<int> downvotedList;
            int idFromSession = (int)Session["id"];



            if (Session["upvoted"] != null)
            {
                List<int> dList = (List<int>)Session["upvoted"];
                if (dList.Contains(idFromSession))
                {
                    votePower = 2;
                }
            }


            if (Session["downvoted"] != null)
            {
                downvotedList = (List<int>)Session["downvoted"];
                if (downvotedList.Contains(idFromSession))
                {
                    return View("VotedAlready");
                }
                else
                {

                    //ViewBag.id = null;

                    CharacterCard card = db.CharacterCards
              .Where(c => c.Id == idFromSession).FirstOrDefault();

                    //card.Rating += 1;
                    card.Votes -= votePower;

                    db.SaveChanges();

                    downvotedList = (List<int>)Session["downvoted"];
                    downvotedList.Add(idFromSession);
                    Session["downvoted"] = downvotedList;

                    //return RedirectToAction("Index", );
                    return View("Upvote");

                }

            }
            else
            {// list is null

                //ViewBag.id = null;
                CharacterCard card = db.CharacterCards
              .Where(c => c.Id == idFromSession).FirstOrDefault();

                //card.Rating += 1;


                card.Votes += votePower;

                db.SaveChanges();

                //upvotedList = (List<int>)Session["upvoted"];
                downvotedList = new List<int>();
                downvotedList.Add(idFromSession);
                Session["downvoted"] = downvotedList;

                //return RedirectToAction("Index", );
                return View("Downvote");
            }
        }*/




        public ActionResult Edit2(int id)
        {
            if (ViewBag.id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CharacterCard characterCard = db.CharacterCards.Find(ViewBag.id);
            CharacterCardViewModel vm = new CharacterCardViewModel();
            if (characterCard == null)
            {
                return HttpNotFound();
            }

           
           
        



            return View(characterCard);
        }


        // POST: CharacterCard/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "Id,ImieNazwisko,Narodowosc,Rasa,Profesja,Archetyp,Krzepa,Zrecznosc,Przenikliwosc,Opanowanie,Charyzma,Umiejetnosci")] CharacterCard characterCard)
        public ActionResult Edit(string Name, int Id, string[] formItem)
        {

            var r = Request;

            CharacterCard characterCard = db.CharacterCards.Find(Id);
            if (ModelState.IsValid)
            {
                characterCard.Name = Name;
                var fItem = Request["formItem.Value"];
                var items = fItem.Split(',');
                var values = items.Select(i => new CharacterCardFormValue(characterCard.Id, "name", i,characterCard)).ToList();

                //db.Entry(characterCard).State = EntityState.Deleted; 
                //db.Entry(characterCard).State = EntityState.Modified;

                //characterCard.CharacterCardFormValues.Clear();

                //db.Entry(characterCard.CharacterCardFormValues).

                //db.SaveChanges();
                //characterCard.CharacterCardFormValues = values;
                //db.Entry(characterCard).State = EntityState.Modified;
                //TryUpdateModel(characterCard);
                //db.Entry(characterCard).State = EntityState.Modified;

                for (var i = 0; i < characterCard.CharacterCardFormValues.Count; i++) {

                    characterCard.CharacterCardFormValues[i].Value = values[i].Value;

                }

                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(characterCard);
        }

        // GET: CharacterCard/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CharacterCard characterCard = db.CharacterCards.Find(id);
            if (characterCard == null)
            {
                return HttpNotFound();
            }
            return View(characterCard);
        }

        // POST: CharacterCard/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CharacterCard characterCard = db.CharacterCards.Find(id);
            db.CharacterCards.Remove(characterCard);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
