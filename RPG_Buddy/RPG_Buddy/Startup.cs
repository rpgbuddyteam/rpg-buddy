﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RPG_Buddy.Startup))]
namespace RPG_Buddy
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
