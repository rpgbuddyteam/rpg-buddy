﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RPG_Buddy.Models
{
    public class ItemCardViewModel
    {
        public string ImieNazwisko { get; set; }
        public string ItemTemplate { get; set; }
        public List<string> ItemTemplateNames { get; set; }
    }
}