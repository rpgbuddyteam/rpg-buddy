﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RPG_Buddy.Models
{
    public class ItemVote
    {
        
        public int ItemVoteId { get; set; }
        public int Value { get; set; }

        public int ItemCardId { get; set; }
        //public int CharacterCardId { get; set; }


        public string UserId { get; set; }

        public ItemVote() { }

        public ItemVote(int Value, int ItemCardId, string UserId) {
            this.Value = Value;
            this.ItemCardId = ItemCardId;
            this.UserId = UserId;
        }


    }
}