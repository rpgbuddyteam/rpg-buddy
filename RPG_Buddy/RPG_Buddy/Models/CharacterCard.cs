﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RPG_Buddy.Models
{
    public class CharacterCard
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual List<CharacterCardFormValue> CharacterCardFormValues { get; set; }
        
        public int Rating { get; set; }
        public virtual List<CharacterVote> Votes { get; set; }

        public CharacterCard()
        {

            Votes = new List<CharacterVote>();
        }

    }
}