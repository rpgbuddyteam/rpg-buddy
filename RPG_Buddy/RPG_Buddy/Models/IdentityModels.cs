﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace RPG_Buddy.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public byte[] UserPhoto { get; internal set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            Database.SetInitializer<ApplicationDbContext>(new DropCreateDatabaseIfModelChanges<ApplicationDbContext>());
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<CharacterCard> CharacterCards { get; set; }

        public DbSet<CharacterTemplate> CharacterTemplates { get; set; }

        public DbSet<Ruleset> Rulesets { get; set; }

        public DbSet<CharacterCardFormValue> CharacterCardFormValues { get; set; }

        public DbSet<AdventureCard> AdventureCards { get; set; }

        public DbSet<ItemTemplate> ItemTemplates { get; set; }

        public DbSet<ItemCard> ItemCards { get; set; }

        public DbSet<ItemVote> ItemVotes { get; set; }

        public DbSet<CharacterVote> CharacterVotes { get; set; }

        public class ApplicationUser : IdentityUser
        {
            // Here we add a byte to Save the user Profile Picture
            public byte[] UserPhoto { get; set; }

        }
    }
}
