﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RPG_Buddy.Models
{
    public class ItemCardFormValue
    {
        public int Id { get; set; }
        public int ItemCardId { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public virtual ItemCard ItemCard { get; set; }
    }
}