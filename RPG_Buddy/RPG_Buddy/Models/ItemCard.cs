﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RPG_Buddy.Models
{
    public class ItemCard
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual List<ItemCardFormValue> ItemCardFormValues { get; set; }

        public int Rating { get; set; }
        public List<ItemVote> Votes { get; set; }


    }
}