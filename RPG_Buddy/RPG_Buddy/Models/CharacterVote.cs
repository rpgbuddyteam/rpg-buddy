﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RPG_Buddy.Models
{
    public class CharacterVote
    {
        public int CharacterVoteId { get; set; }
        public int Value { get; set; }

        
        public int CharacterCardId { get; set; }


        public string UserId { get; set; }

        public CharacterVote() { }

        public CharacterVote(int Value, int ItemCardId, string UserId)
        {
            this.Value = Value;
            this.CharacterCardId = ItemCardId;
            this.UserId = UserId;
        }


    }
}