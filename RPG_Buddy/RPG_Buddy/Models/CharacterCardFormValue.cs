﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RPG_Buddy.Models
{
    public class CharacterCardFormValue
    {
        public CharacterCardFormValue( int characterCardId, string name, string value)
        {
            CharacterCardId = characterCardId;
            Name = name;
            Value = value;
        }

        public CharacterCardFormValue() { }

        public CharacterCardFormValue(int characterCardId, string name, string value, CharacterCard characterCard)
        {
            CharacterCardId = characterCardId;
            Name = name;
            Value = value;
            CharacterCard = characterCard;
        }

        public int Id { get; set; }
        public int CharacterCardId { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public virtual CharacterCard CharacterCard { get; set; }

        

    }
}