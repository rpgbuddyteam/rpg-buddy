﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RPG_Buddy.Models
{
    public class ItemTemplate
    {
        [Key]
        public int Id { get; set; }
        public string Schema { get; set; }
        public string Name { get; set; }
    }
}