﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RPG_Buddy.Models
{
    public class CharacterCardViewModel
    {
        public string ImieNazwisko { get; set; }
        public string CharacterTemplate { get; set; }
        public List<string> CharacterTemplateNames { get; set; }


    }
}