﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RPG_Buddy.Models
{
    public class AdventureCard
    {
        public int Id { get; set; }
        public string OpisPrzygody { get; set; }
        public string Postać1 { get; set; }
        public string Postać2 { get; set; }
        public string Postać3 { get; set; }
        public string Postać4 { get; set; }
        public string Przedmiot1 { get; set; }
        public string Przedmiot2 { get; set; }
        public string Przedmiot3 { get; set; }
        public string Przedmiot4 { get; set; }
        public string Przedmiot5 { get; set; }
        public string Przedmiot6 { get; set; }
        public string Przedmiot7 { get; set; }
        public string Przedmiot8 { get; set; }
        public string Przedmiot9 { get; set; }
        public string Przedmiot10 { get; set; }
        public List<ApplicationUser> Users { get; set; }
        public List<ItemCard> Items { get; set; }
    }
}